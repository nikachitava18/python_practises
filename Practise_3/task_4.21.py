# task 4.21
# შეიტანეთ ოთხი a1, a2, a3, a4 მთელი რიცხვი,
# რომლებიდანაც სამი ერთმანეთის ტოლია. დაბეჭდეთ განსხავავებული რიცხვი.

a1 = int(input())
a2 = int(input())
a3 = int(input())
a4 = int(input())

if a1 == a2 == a3 != a4:
    print(a4)
elif a1 == a2 == a4 != a3:
    print(a3)
elif a1 == a3 == a4 != a2:
    print(a2)
else:
    print(a1)