# task_4.6
# შეიტანეთ სამი რიცხვი დაბეჭდეთ ამ რიცხვებს შორის უდიდესის კვადრატი (გაითვალისწინეთ ფორმატი).

x = int(input())
y = int(input())
z = int(input())

if x > y > z:
    print(x ** 2)
elif y > x > z:
    print(y ** 2)
else:
    print(z ** 2)

