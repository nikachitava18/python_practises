# task 4.11
# შეიტანეთ მთელი რიცხვი, თუ რიცხვი არის უარყოფითი გამოვიტანოთ შეტყობინება „The number is negative“,
# თუ რიცხვი ტოლია 0-ის გამოვიტანოთ შეტყობინება „The number equal to zero“,
# თუ რიცხვი არის დადებითი გამოვიტანოთ შეტყობინება „The number is positive“.

a = int(input())
if a < 0:
    print("The number is negative")
elif a == 0:
    print("The number equal to zero")
elif a > 0:
    print("The number is positive")
