# task 4.19
# შეიტანეთ n მთელი რიცხვი, დაბეჭდეთ n-ის 10-ზე გაყოფის შედეგად მიღებული ნაშთი თუ n ლუწია,
# დაბეჭდეთ n-ის 10-ზე გაყოფის შედეგად მიღებული მთელი ნაწილი თუ n კენტია.

n = int(input())

if (n % 2) == 0:
    print(n % 10)  # ნაშთი
else:
    print(n // 10)  # მთელი ნაწილი

