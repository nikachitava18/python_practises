# task_3
# შეიტანეთ პროგრამაში მთელი დადებითი რიცხვები მანამ, სანამ მათი ნამრავლი არ გადააჭარბებს 200-ს.

import math

x = []
multiplied = 0

while multiplied < 200:
    x.append(int(input("Enter number: ")))
    multiplied = math.prod(x)

