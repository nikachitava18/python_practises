# task_2.5

# განსაზღვრეთ x და y ცვლადები, მიანიჭეთ მნიშვნელობა კლავიატურიდან, ცალ-ცალკე
# სტრიქონზე დაბეჭდეთ x-ის y-ზე გაყოფის შედეგად მიღებულ მთელი შედეგი და y-ის x-ზე
# გაყოფის შედეგად მიღებული ნაშთი. (ფორმატის გათვალისწინებით).

x, y = 10, 5
print(str(x)+"/"+str(y)+"="+str(x/y))
print(str(y)+"/"+str(x)+"="+str(y%x))
