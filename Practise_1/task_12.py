# task_12

# განსაზღვრეთ საჭირო რაოდენობის ცვლადები, მიანიჭეთ რაიმე მნიშვნელობები
# კლავიატურიდან და გამოიტანეთ ქვემოთ მოყვანილი გამოსახულებების გამოთვლების შედეგი:


a = 2
print(3*a+6)

# task_12.2
a, b = 3, 1
print((3*a)+(6*b)-1)

# task_12.3
k, t, z = 5, 4, 9
print(k+t-z+12)

# task_12.4
a, t = 4, 2
print((3*a)/4+6*t)

# task_12.5
h, d = 2, 3
print((-4*h)+(3*d)-4)

# task_12.6
a, b = 5, 1
print((3*a)+6 / 4 - (3*b))

# task_12.7
a, c = 6, 2
print((7*a**2)+(6*c))

# task_12.8
a, c = 4, 3
example = (2*a**2) + (6*c**3)
print(example)

# task_12.9
k, t, p, l = 2, 3, 4, 5
example_1 = (11*k) + (2*t) - p + (12*l)
print(example_1)

# task_12.10
k, l, m = 2, 9, 8
example_2 = k + (12*l)/(4*m)
print(example_2)