"""""
პროგრამაში შეტანილი მილიმეტრები გადაიყვანეთ 
(А)-სანტიმეტრებში და მილიმეტრებში; 
(B)-დეციმეტრებში, სანტიმეტრებში და მილიმეტრებში; 
(C)-მეტრებში, დეციმეტრებში, სანტიმეტრებში და მილიმეტრებში.
"""""

mm = float(input("Enter millimeters: "))
a = input("Enter needed symbol: ")

if a == "A":
    centimeter = mm / 10
    print(str(centimeter)+" centimter "+ str(mm) + " millimeters ")
elif a == "B":
    decimeter = mm / 100
    centimeter = mm / 10
    print(str(decimeter)+" decimeter "+str(centimeter)+" centimter "+str(mm)+" millimeters ")
elif a == "C":
    meter = mm / 1000
    decimeter = mm / 100
    centimeter = mm / 10
    print(str(meter)+" meter "+str(decimeter)+" decimeter "+str(centimeter)+" centimter "+str(mm)+" millimeters ")
else:
    print("Undefined")