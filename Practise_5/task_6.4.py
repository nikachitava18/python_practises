"""""
პროგრამას შეტანილი ლარი გადაჰყავს სხვა ვალუტაში, მოთხოვნის შესაბამისად 
(D)-დოლარში, (E)-ევრო, (S)-სტერლინგი და (M)-მანეთი და პირიქით. 
გააფორმეთ პროგრამა ვიზუალურად, გაცვლითი კურსი პროგრამაში განსაზღვრეთ წინასწარ, პროგრამულად.
"""""

a = float(input("Enter amount of money (GEL): "))
c = input("Enter convert symbol: ")
D = 2.4
E = 2.8
S = 1.7
M = 3.2
value = 0

if c == "D":
    value = a/D
    print(str(a)+" GEL = "+str(value)+" USD")
elif c == "E":
    value = a/E
    print(str(a) + " GEL = " + str(value) + " EUR")
elif c == "S":
    value = a/E
    print(str(a) + " GEL = " + str(value) + " STERLING")
elif c == "M":
    value = a/E
    print(str(a) + " GEL = " + str(value) + " MANETI")
else:
    print("Undefined")

