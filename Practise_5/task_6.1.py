"""""
შეიტანეთ მთელი რიცხვი, დაბეჭდეთ რიცხვის შესაბამისი კვირის დღე. მაგ: (1-Monday, 2-Tuesday, …). 
თუ რიცხვი არ წარ¬მო¬ადგენს კვირის დღეს, გამოიტანეთ შეცდომის შესახებ შეტ¬ყობინება.
"""""

x = int(input("Enter the number: "))
if x == 1:
    print("Monday")
elif x == 2:
    print("Tuesday")
elif x == 3:
    print("Wednesday")
elif x == 4:
    print("Thursday")
elif x == 5:
    print("Friday")
elif x == 6:
    print("Saturday")
elif x == 7:
    print("Sunday")
else:
    print("Undefined")
