"""""
შეტანილი თვის ნომრის მიხედვით დაბეჭდეთ წლის რომელ დროს ეკუთვნის შესაბამისი თვე.
გააფორმეთ პროგრამა ვიზუალურად.
"""""

x = int(input("Enter month number: "))

if x == 1:
    print("Winter")
elif x == 2:
    print("Winter")
elif x == 3:
    print("Spring")
elif x == 4:
    print("Spring")
elif x == 5:
    print("Spring")
elif x == 6:
    print("Summer")
elif x == 7:
    print("Summer")
elif x == 8:
    print("Summer")
elif x == 9:
    print("Autumn")
elif x == 10:
    print("Autumn")
elif x == 11:
    print("Autumn")
elif x == 12:
    print("Winter")
else:
    print("Undefined")