""""
პროგრამაში შეტანილი წამები გადაიყვანეთ შესაბამისად 
(H)-საათებში; (S)-საათებში და წამებში; (M)-საათებში, 
წუთებში და წამებში; (D)-დღეებში, საათებში, წუთებში და წამებში.
"""""

sec = float(input("Enter number of seconds: "))
a = str(input("Enter needed symbol: "))

if a == "H":
    hours = sec / 3600
    print(str(hours)+" hours")
elif a == "S":
    hours = sec / 3600
    minutes = sec / 60
    print(str(hours)+" hours "+str(minutes)+" minutes")
elif a == "M":
    hours = sec / 3600
    minutes = sec / 60
    print(str(hours) + " hours " + str(minutes) + " minutes "+str(sec)+" seconds")
elif a == "D":
    days = sec / 86400
    hours = sec / 3600
    minutes = sec / 60
    print(str(days)+" days "+str(hours) + " hours " + str(minutes) + " minutes " + str(sec) + " seconds")
else:
    print("Undefined")
