"""""
შეტანილი სამნიშნა მთელი რიცხვის შესაბამისად დაბეჭდეთ თუ რომელი სატელეფონო ოპერატორია  ეს რიცხვი. 
მაგალითად,  (599 – მაგთი, 577–ჯეოსელი, . . .). 
თუ რიცხვი არ არის რომელიმე ოპერატორი, გამოიტანეთ შეცდომის შესახებ შეტ-ყობინება.
"""""

x = int(input("Enter the number: "))

if x == 599:
    print("Magti")
elif x == 577:
    print("Geocell")
elif x == 568:
    print("Beeline")
elif x == 571:
    print("Beeline")
elif x == 597:
    print("Beeline")
else:
    print("Undefined")
