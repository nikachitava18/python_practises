""""
შეიტანეთ მთელი რიცხვი, დაბეჭდეთ რიცხვის შესაბამისი თვე. მაგ: (1-January, 2-February, . . .). 
თუ რიცხვი არ არის თვე, გამოიტანეთ შეცდომის შესახებ შეტ¬ყობინება.
"""""

x = int(input("Enter the number: "))

if x == 1:
    print("January")
elif x == 2:
    print("February")
elif x == 3:
    print("March")
elif x == 4:
    print("April")
elif x == 5:
    print("May")
elif x == 6:
    print("June")
elif x == 7:
    print("July")
elif x == 8:
    print("August")
elif x == 9:
    print("September")
elif x == 10:
    print("Octomber")
elif x == 11:
    print("November")
elif x == 12:
    print("December")
else:
    print("Undefined")