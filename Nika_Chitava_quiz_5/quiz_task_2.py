# task_2
# შეიტანეთ ინგლისური ანბანის ასოებისგან შემდგარი სტრიქონი,
# სტრიქონში წაშალეთ ყველა ხმოვანი. დაბეჭდეთ თავდაპირველი და
# მიღებული სტრიქონები.

text = "Hello world"
letters = ["a", "i", "e", "o", "u"]
new_text = text

for i in text.lower():
    if i in letters:
        new_text = new_text.replace(i,"")
print(f"თავდაპირველი: {text}, მიღებული: {new_text}")
