"""""
შეიტანეთ სამი რიცხვი, განსაზღვრეთ გამოდგება თუ არა ისინი სამკუთხედის გვერდების
ზომებად. თუ გამოდგება, გამოთვალეთ მისი ფართობი ჰერონის ფორმულით, წინააღმდეგ
შემთხვევაში გამოიტანეთ შეტყობინება „False“
"""""

import math

a = float(input("a=>"))
b = float(input("b=>"))
c = float(input("c=>"))

if a > 0 and b > 0 and c > 0 and a + b > c and a + c > b and b + c > a:
    p = (a+b+c)/2
    s = math.sqrt(p*(p-a)*(p-b)*(p-c))
    print(s)
else:
    print("False")