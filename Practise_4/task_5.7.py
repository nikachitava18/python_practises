"""""
შეიტანეთ სამი წერტილის კოორდინატები, განსაზღვრეთ გამოდგება თუ არა ისინი
სამკუთხედის წვეროებად. თუ გამოდგება, დაადგინეთ სამკუთხედის ტიპი გვერდების და
კუთხეების მიხედვით, წინააღმდეგ შემთხვევაში გამოიტანეთ შეტყობინება „False“ (გაითვალისწინეთ ფორმატი).
"""""
import math

x1 = float(input("x1=>"))
y1 = float(input("y1=>"))

x2 = float(input("x2=>"))
y2 = float(input("y2=>"))

x3 = float(input("x3=>"))
y3 = float(input("y3=>"))

a = math.sqrt(pow(x2-x1, 2)+pow(y2-y1, 2))
b = math.sqrt(pow(x3-x2, 2)+pow(y3-y2, 2))
c = math.sqrt(pow(x3-x1, 2)+pow(y3-y1, 2))

if a > 0 and b > 0 and c > 0 and a + b > c and a + c > b and b + c > a:
    if pow(a, 2) + pow(b, 2) == pow(c, 2):
        print("სამკუთხედი მართკუთხაა")
    if pow(a, 2) + pow(b, 2) < pow(c, 2):
        print("სამკუთხედი ბლაგვკუთხაა")
    if pow(a, 2) + pow(b, 2) > pow(c, 2):
        print("სამკუთხები მახვილკუთხაა")
    if a != b and a != c and b != c:
        print("სამკუთხედი სახვადასხვა გვერდაა")
    if a == b == c:
        print("სამკუთხედი ტოლგვერდაა")
    if a == b or a == c or b == c:
        print("სამკუთხედი ტოლფერდაა")
else:
    print("False")


