# quiz4_task2

# შეიტანეთ N რაოდენობის მთელი რიცხვი, იპოვეთ შეტანილი რიცხვებიდან
# 3-ის ჯერადი დადებითი რიცხვების ჯამის ციფრების საშუალო
# არითმეტიკული.

N = int(input("How many numbers: "))
summary = 0
count = 0

for _ in range(N):
    x = int(input("Enter number: "))

    if (x % 3) == 0:
        summary = summary + x
        count = count + 1

print(f"3-ის ჯერადი ციფრების ჯამი: {summary}")
print(f"3-ის ჯერადი ციფრების ჯამის საშუალო: {summary/count}")

