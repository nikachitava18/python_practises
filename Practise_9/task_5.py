# task_5
# განსაზღვრეთ 8 ელემენტიანი მთელ რიცხვთა სია,
# ელემენტების ინიციალიზაცია მოახდინეთ პროგრამაში.
# დაბეჭდეთ ლუწ ინდექსიანი ელემენტები და ლუწ ინდექსიანი ელემენტების ნამრავლი.

import random

list = []

for _ in range(8):
    list.append(random.randint(10, 250))

# print(list)

for i in range(2, 8, 2):
    print(list[i])

print("ჯამი: ", list[2]+list[4]+list[6])