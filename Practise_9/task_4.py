# task_4
# განსაზღვრეთ 8 ელემენტიანი მთელ რიცხვთა სია,
# ელემენტების ინიციალიზაცია მოახდინეთ პროგრამაში.
# დაბეჭდეთ კენტ ინდექსიანი ელემენტები და კენტ ინდექსიანი ელემენტების ჯამი.

import random

list = []

for _ in range(8):
    list.append(random.randint(10, 250))

# print(list)

for i in range(1, 8, 2):
    print(list[i])

print("ჯამი: ", list[1]+list[3]+list[5]+list[7])


