# task_6
# განსაზღვრეთ 8 ელემენტიანი მთელ რიცხვთა სია,
# ელემენტების ინიციალიზაცია მოახდინეთ პროგრამაში.
# დაბეჭდეთ კენტ ინდექსიანი ელემენტებისა და ლუწ ინდექსიანი ელემენტები ჯამი ცალ-ცალკე სტრიქონზე,
# მიღებული ჯამებიდან დაბეჭდეთ უდიდესი.

import random

list = []

sum_even_index = 0
sum_odd_index = 0

for _ in range(8):
    list.append(random.randint(10, 250))

# print(list)

for i in range(1, 8):

    if (i % 2) == 0:
        sum_even_index = sum_even_index + list[i]
        # print(f"ლუწ ინდექსიანი: {list[i]}")
    else:
        sum_odd_index = sum_odd_index + list[i]
        # print(f"კენტ ინდექსიანი: {list[i]}")

print(f"ლუწ ინდექსიანების ჯამი: {sum_even_index}")
print(f"კენტ ინდექსიანების ჯამი: {sum_odd_index}")

