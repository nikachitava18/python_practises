# task_3
# განსაზღვრეთ 4 ელემენტიანი მთელ რიცხვთა სია,
# ელემენტების ინიციალიზაცია მოახდინეთ პროგრამაში.
# დაბეჭდეთ სიის ელემენტების ჯამი და ნამრავლი.

import math
import random

list = []

for _ in range(4):
    list.append(random.randint(10, 250))

# print(list)
print("ჯამი:", sum(list))
print("ნამრავლი", math.prod(list))