# task_18

# პირველი 15 კენტი ნატურალური რიცხვი ჩაწერეთ შესაბამის სიაში.
# დაბეჭდეთ სიის სამის ჯერადი ელემენტების ნამრავლი.

x = []
mult = 1

for i in range(0, 30, 2):
    x.append(i+1)
print(x)

for idx in x:
    if(idx % 3) == 0:
        mult = mult * idx
print(f"ნამრავლი: {mult}")