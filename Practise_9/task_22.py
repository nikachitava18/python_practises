# task_22

# განსაზღვრეთ 12 ელემენტიანი ნამდვილი ტიპის სია
# ელემენტებს მნიშვნელობები მიანიჭეთ კლავიატურიდან ციკლის საშუალებით.
# დაითვალეთ დადებითი და უარყოფითი ელემენტების რაოდენობა.

x = []

count_negative = 0
count_positive = 0

for _ in range(12):
    x.append(int(input("Enter number: ")))

for i in x:
    if i > 0:
        count_positive += 1
    else:
        count_negative += 1
print(f"დადებითი ელემენტების რაონდეობა {count_positive}, უარყოფითი {count_negative}")