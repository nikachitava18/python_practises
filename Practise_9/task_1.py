# task_1
# განსაზღვრეთ 7 ელემენტიანი ნამდვილ რიცხვთა სია,
# ელემენტების ინიციალიზაცია მოახდინეთ პროგრამაში.
# დაბეჭდეთ სიის მეორე, მეოთხე, მეექვსე ელემენტები.

import random

numbers = []

for _ in range(7):
    numbers.append(random.randint(10, 200))

# print(numbers)
print(numbers[1], numbers[3], numbers[5])