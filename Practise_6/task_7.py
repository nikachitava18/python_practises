# task_7

# შეიტანეთ a მთელი რიცხვი, თუ a ნაკლებია 20-ზე, დაბეჭდეთ ყველა მთელი რიცხვი a-დან 20-ის ჩათვლით.

a = int(input("Enter a number: "))

if a < 20:
    for i in range(a, 20+1):
        print(i)