# task_19
# შეიტანეთ 8 მთელი რიცხვი for ციკლის საშუალებით, დაბეჭდეთ კენტი რიცხვების რაოდენობა.

x = []
odd_number = 0

for i in range(8):
    x.append(int(input("Enter number: ")))

if (x[0] % 2) == 0:
    odd_number += 1
if (x[1] % 2) == 0:
    odd_number += 1
if (x[2] % 2) == 0:
    odd_number += 1
if (x[3] % 2) == 0:
    odd_number += 1
if (x[4] % 2) == 0:
    odd_number += 1
if (x[5] % 2) == 0:
    odd_number += 1
if (x[6] % 2) == 0:
    odd_number += 1
if (x[7] % 2) == 0:
    odd_number += 1
print(f"ამ რიცხვებიდან {odd_number} რიცხვი არის კენტი")