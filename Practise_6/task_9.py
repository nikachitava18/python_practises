# task_9
# შეიტანეთ a  და b მთელი რიცხვი,
# თუ a მეტია b-ზე, დაბეჭდეთ ყველა მთელი რიცხვი b-დან a-მდე ჩათვლით.
# თუ a ნაკლებია b-ზე, დაბეჭდეთ ყველა მთელი რიცხვი a-დან b-მდე ჩათვლით.

a = int(input("Enter a number: "))
b = int(input("Enter b number: "))

if a > b:
    for i in range(b, a+1):
        print(i)

elif a < b:
    for i in range(a, b+1):
        print(i)
else:
    print("Undefined")


