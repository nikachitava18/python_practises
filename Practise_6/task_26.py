# task_26
# შეიტანეთ 12 მთელი რიცხვი for ციკლის საშუალებით,
# დაბეჭდეთ შეტანილი ლუწი რიცხვების ჯამი, კენტი რიცხვების ნამრავლი და სამის ჯერადი რიცხვების რაოდენობა.

import math

x = []
even_numbers = []
odd_numbers = []
multiplied_numbers = []


for i in range(12):
    x.append(int(input("Enter number: ")))

for num in x:
    if num % 2 == 0:
        even_numbers.append(num)
    if num % 2 == 1:
        odd_numbers.append(num)
    if num % 3 == 0:
        multiplied_numbers.append(num)

print("ლუწი რიცხვების ჯარმი", sum(even_numbers))
if len(odd_numbers) == 0:
    print("კენტი რიცხვების ნამრავლი 0")
else:
    print("კენტი რიცხვების ნამრავლი", math.prod(odd_numbers))
print("3-ის ჯერადი", len(multiplied_numbers))
