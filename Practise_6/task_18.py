# task_18
# შეიტანეთ 6 მთელი რიცხვი for ციკლის საშუალებით, დაბეჭდეთ ლუწი რიცხვების რაოდენობა.

x = []
even_number = 0

for i in range(6):
    x.append(int(input("Enter number: ")))

if (x[0] % 2) == 0:
    even_number += 1
if (x[1] % 2) == 0:
    even_number += 1
if (x[2] % 2) == 0:
    even_number += 1
if (x[3] % 2) == 0:
    even_number += 1
if (x[4] % 2) == 0:
    even_number += 1
if (x[5] % 2) == 0:
    even_number += 1
print(f"ამ რიცხვებიდან {even_number} რიცხვი არის ლუწი")