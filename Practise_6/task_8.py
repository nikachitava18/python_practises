
# შეიტანეთ a მთელი რიცხვი,
# თუ a მეტია 20-ზე,
# დაბეჭდეთ ყველა მთელი რიცხვი 20-დან a-მდე ჩათვლით.

a = int(input("Enter number: "))

if a > 20:
    for i in range(20, a+1):
        print(i)