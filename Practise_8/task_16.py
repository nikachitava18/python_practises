# task_16
# შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუალედიდან 15 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშვნელობას გაუცვალეთ ადგილები).
# დაითვალოთ რამდენია გამოტანილ რიცხვებს შორის 5-ის ჯერადი.
import random

count_number = 0

a = int(input("Enter number: "))
b = int(input("Enter number: "))

for _ in range(15):
    if a > b:
        x = random.randint(b,a)
        if (x % 5) == 0:
            count_number += 1
        print(x)
    else:
        x = random.randint(a,b)
        if (x % 5) == 0:
            count_number += 1
        print(x)
print(f"5-ის ჯერადი რიცხების რაოდენობა: {count_number}")

