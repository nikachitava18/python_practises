# task_9
# გამოიტანეთ სამი შემთხვევითი მთელი რიცხვი [0; n] შუალე¬დიდან, n-ის შეტანა მოახდინეთ კლავიატურიდან.

import random

n = int(input("Enter number: "))

for _ in range(3):
    x = random.randint(0, n)
    print(x)