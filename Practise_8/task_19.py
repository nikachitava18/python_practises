# task_19
# შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუალედიდან 40 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშვნელობას გაუცვალეთ ადგილები).
# დაადგინეთ რამდენი ლუწი და რამდენი კენტი რიცხვია გამოტანილ რიცხვებს შორის,
# გამოიტანეთ ლუწი და კენტი რიცხვების ჯამი, მიღებულ ჯამებს შორის გამოიტანეთ 5 შემთხვევითი რიცხვი.

import random

a = int(input("Enter number: "))
b = int(input("Enter number: "))

even_number = 0
odd_number = 0
sum_even_numbers = 0
sum_odd_numbers = 0

for _ in range(40):
    if a > b:
        x = random.randint(b, a)
        print(x)
        if ( x % 2) == 0:
            even_number += 1
            sum_even_numbers = sum_even_numbers + x
        else:
            odd_number += 1
            sum_odd_numbers = sum_odd_numbers + x
    else:
        x = random.randint(a, b)
        print(x)
        if (x % 2) == 0:
            even_number += 1
            sum_even_numbers = sum_even_numbers + x
        else:
            odd_number += 1
            sum_odd_numbers = sum_odd_numbers + x

print(f"Even numbers: {even_number}, Odd numbers: {odd_number}")
print(f"Even numbers summary: {sum_even_numbers}, Odd numbers summary: {sum_odd_numbers}")


