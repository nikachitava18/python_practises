# task_18
# შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუალედიდან 40 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშ¬ვნელობას გაუცვალეთ ადგილები).
# დაადგინეთ რამდენი რიცხვია (a+b)/2-ზე მეტი.

import random

a = int(input("Enter number: "))
b = int(input("Enter number: "))

count = 0

for _ in range(40):
    if a > b:
        x = random.randint(b, a)
        print(x)
        if x > (a+b)/2:
            count += 1
    else:
        x = random.randint(a, b)
        print(x)
        if x > (a+b)/2:
            count += 1

print(f"მეტი რიცხვების რაოდენობა: {count}")