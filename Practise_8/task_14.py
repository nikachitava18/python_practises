# task_14
# გამოიტანეთ სამი შემთხვევითი მთელი რიცხვი [m; n] შუალედიდან, m-ის და n-ის შეტანა მოახდინეთ კლავიატურიდან.

import random

m = int(input("Enter number: "))
n = int(input("Enter number: "))

for _ in range(3):
    if m < n:
        x = random.randint(m, n)
        print(x)
    else:
        x = random.randint(n, m)
        print(x)