# task_15
# შეიტანეთ a და b მთელი რიცხვები,
# თუ a ნაკლებია b-ზე დაბეჭდეთ 10 შემთხვევითი რიცხვი [a; b] შუალედიდან,
# თუ b ნაკლებია a-ზე დავბეჭდოთ 10 შემთხვევითი რიცხვი [b; a] შუალედიდან.
import random

a = int(input("Enter number: "))
b = int(input("Enter number: "))

if a < b:
    for _ in range(10):
        x = random.randint(a, b)
        print(x)
else:
    for _ in range(10):
        x = random.randint(b, a)
        print(x)
