# task_3
# განსაზღვრეთ სამი a, b, c მთელი ტიპის ცვლადები,მიანიჭეთ შემთხვევითი რიცხვები.
# დაბეჭდეთ შემდეგი გამოსახულების მნიშვნელობები:
import math
import random

a = random.randint(1, 1000)
b = random.randint(1, 1000)
c = random.randint(1, 1000)

print((a+b)/pow(c, 2))
print((a-b)*(c-b))
print(pow(a, 4) + math.sqrt(b) + c)

