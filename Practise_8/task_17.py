# task_17
# შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუალედიდან 7 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშვნელობას გაუცვალეთ ადგილები).
# დაბეჭდეთ გამოტანილი რიცხვების ჯამი.

import random

summary = 0

a = int(input("Enter number: "))
b = int(input("Enter number: "))

for _ in range(7):
    if a > b:
        x = random.randint(b, a)
        summary = summary + x
        print(x)
    else:
        x = random.randint(a, b)
        summary = summary + x
        print(x)
print(f"Summary: {summary}")

